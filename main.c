#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include "Utils.h"

int i = 0;
char input[214000];
char output[214];
int output_size_left = sizeof(output);
struct couple couples[25];
int length_of_couples_list = 0;
int index;
int MAX_BYTE_VALUE = 255;
short* begin_input;

// Functions
char* doAction(char c, char* array_of_chars);
int importInput();
char integer_to_char(int character) ;
int couple(int i);
int find_corresponding_pointer (int j);

int main()
{
    int amountOfChars;
    //puts("How many characters do you need?");
    //scanf("%d", &amountOfChars);
    amountOfChars=10;
    short* array_of_chars = (short*) malloc(amountOfChars * sizeof(short));
    begin_input = array_of_chars;
    printf("\n%d character places are reserved\n", amountOfChars);

    int status = importInput();
    if (status == 1) {return 1;}
    printf("%s\n", input);
    char* array = (char *) array_of_chars;
    for (index=0; index<strlen(input); ++index) {
        array = doAction(input[index], array);
        printf("Short: %hi\n", array_of_chars[i]);
        fflush(stdout);
    }
    printf("The input was: %s Length: %d\n", input, (int) sizeof(input));
    printf("The output is: %s Length: %d\n", output, (int) sizeof(output));
    int tmp;
    for(tmp=0; tmp < amountOfChars; tmp++){
        printf("%d; ", *(array_of_chars++));
    }
    return 0;
}

char* doAction(char c, char* array_of_chars)
{
    printf("Char: %c\n", c);
    // For case '['
    int end_of_array;
    struct couple a_couple;

    // For case '.'
    char conversed;

    switch(c) {
        case '>':
            ++i;
            array_of_chars++;
            printf("I = %d\n", i);
            break;
        case '<':
            --i;
            array_of_chars--;
            break;
        case '+':
            array_of_chars[i] = (char) ((++(array_of_chars[i])) % MAX_BYTE_VALUE);
            break;
        case '-':
            array_of_chars[i] = (char) ((--(array_of_chars[i])) % MAX_BYTE_VALUE);
            break;
        case '[':
            end_of_array = couple(i);
            a_couple.begin = i;
            if ( end_of_array != -1) {
                a_couple.end = end_of_array;
            } else {
                exit(512);
            }
            couples[length_of_couples_list] = a_couple;
            ++length_of_couples_list;
            break;
        case ']':
            if (array_of_chars[i] == 0) {
                ++i;
                array_of_chars++;
            } else {
                while (input[index] != '[') {
                    index--;
                }
            }
            break;
        case '.':
            conversed = integer_to_char(array_of_chars[i]);
            printf("Output: %c, %c\n", array_of_chars[i], *array_of_chars);
            output[sizeof(output) - output_size_left] = conversed;
            output_size_left--;
            break;
        case ',':
            // Make it possible to enter an integer value
            puts("Please enter a character\n");
            array_of_chars[i] = (char) getchar();
            break;
        default:
            break;
    }
    return array_of_chars;
}

/**
 * Get a filename and read the contents of that file
 */
int importInput() {
/**
 *  Read from file
 */
    char input_method[214000], file_name[]="brainfuck", extention[5] = ".txt";
    //puts("What is the name of the input file?");
    //scanf("%s", file_name);
    if (strcmp(file_name + strlen(file_name) - 4, ".txt")) {
        strcat(file_name, extention);
    };
    if(access(file_name, F_OK ) == -1) {
        printf("%s does not exists or is in the wrong directory\n", file_name);
        return 1;
    }
    FILE * fP;
    fP = fopen(file_name, "r");
    char singleLine[214000];
    while(!feof(fP)) {
        memset(singleLine, 0, sizeof(singleLine));
        fgets(singleLine, sizeof(singleLine) ,fP);
        char *pos, newline[] = "\n";
        if (strstr(singleLine, newline) != NULL) {
            int i;
            for (i = 0; i < strlen(singleLine);i++) {
                if (singleLine[i] == '\\' && singleLine[i+1] == 'n') {
                    singleLine[i] = (char) 0;
                    singleLine[i+1] = (char) 0;
                }
            }
        }
        // Remove the newline chars from the string
        if ((pos=strchr(singleLine, '\n')) != NULL) {
            *pos = '\t';
        } else if ((pos=strchr(singleLine, '\0')) != NULL)  {
            *pos = '\t';
        }

        strcat(input_method, singleLine);
    }
    fclose(fP);
    strcat(input, input_method);
    return 0;
}

int couple(int i) {

    struct couple ekoppel;
    // variable die je meekrijgt
    int begin_of_another_array = 0;
    bool found = 0;
    while (1) {
        if (input[i] == '[') {
            ekoppel.begin = i;
            i++;
            while (!found) {
                if (input[i] == '[') {
                    begin_of_another_array++;
                } else if (input[i] == ']' && begin_of_another_array > 0)  {
                    begin_of_another_array--;
                } else if (input[i] == ']' && begin_of_another_array == 0)  {
                    found = 1;
                }
                i++;
            }
            ekoppel.end = --i;
            break;
        }
        i++;
    }

    printf("\tBegin: %d \n\tEnd: %d\n", ekoppel.begin, ekoppel.end);
    return ekoppel.end;

}

char integer_to_char(int character) {
    return (char) character;
}

/**
 * This function returns the corresponding square bracket.
 * The inte   ger argument is the end bracket.
 */
int find_corresponding_pointer (int j) {
    int i;

    for (i=0; i < sizeof(couples); i++) {
        if (couples[i].end == j) {
            printf("\n%d\n", couples[i].begin);
            return couples[i].begin;
        }
    }
    return -1;
}
